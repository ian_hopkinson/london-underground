#!/usr/bin/env python
# encoding: utf-8

from __future__ import unicode_literals
import unittest
from nose.tools import assert_equal, assert_almost_equal

from process import(get_base_data, get_lat_long, get_circuit_diagram_from_svg,
                    get_circuit_diagram_from_csv, get_station_depths,
                    get_average_depth_from_raw)

class LondonUndergroundTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.base_data = get_base_data("LondonUndergroundStations.csv")
        cls.lat_long = get_lat_long("openstreetmap_data.csv")
        cls.circuit_diagram = get_circuit_diagram_from_svg("tubestations.json")
        cls.station_depths = get_station_depths("station-depths.csv")

    #def test_load_raw_output(self):
    #    assert_equal(expected, observed)
    #    pass
    def test_loads_circuit_diagram_from_csv(self):
        circuit_diagram = get_circuit_diagram_from_csv('digitiser')
        print circuit_diagram[0:10]

    def test_get_station_depths(self):
        station_depths = get_station_depths('station-depths.csv')
        print station_depths[0:10]

    def test_get_average_depth_from_raw(self):
        # One that has a depth for Waterloo and city 85.025 - Waterloo
        assert_almost_equal(18.975, get_average_depth_from_raw(self.station_depths.ix[250]))
        # Has just one depth, so easy averaging
        assert_almost_equal(5.5, get_average_depth_from_raw(self.station_depths.ix[0]))
        # Another one with multiple depths - Baker Street
        assert_almost_equal(14.025, get_average_depth_from_raw(self.station_depths.ix[9]))

    def test_get_average_depth_from_raw_handles_zero(self):
        # One that has a depth for Waterloo and city 85.025 - Waterloo
        assert_almost_equal(0.0, get_average_depth_from_raw(self.station_depths.ix[247]))