#!/usr/bin/env python
# encoding: utf-8
import codecs
import csv
import glob
import json
import math
import numpy
import pandas
import re
import sys

from os.path import join, split

# todo:
# 1. We're losing zone info on import to Tableau
# 9. stations-depth.csv Waterloo data currently comes out as space when not a value, other columns are NaN
# 10. Ideally we want a single file, rather than separate data and path files
# Add in Waterloo and City Line on path
# Add survey data to path (Station and Line are space padding in survey, and multiple lines per path entry)

# Opening dates for underground lines:
opening_dates = {"Bakerloo": 1906 , "Central": 1900 , "Circle": 1884, 
                 "District": 1868, "Hammersmith & City": 1990,
                  "Jubilee": 1979, "Metropolitan": 1863,
                  "Northern": 1890, "Piccadilly": 1906, "Victoria": 1968 , 
                  "Waterloo & City": 1898}

# Sources
# Paris metro
# London Underground 1
# http://frenchflairdata.blogspot.co.uk/2012/08/the-development-of-london-tube.html
# http://www.tableausoftware.com/public/gallery/history-london-underground
# London Underground 2
# http://public.tableausoftware.com/profile/mingda#!/vizhome/LondonTube/BIofLondonUndergroundpassengercounts

# London underground data
# https://www.whatdotheyknow.com/request/usage_of_london_underground_dlr
# http://www.tfl.gov.uk/corporate/publications-and-reports/london-travel-demand-survey

OUTPUT_COLUMNS = ["Station", "Lines", "First Line", "nlines", "Lat", "Long",
                  "x", "y", "DateOpened", "LocalAuthority", "Zone", "Usage"]

line_names = set(["Bakerloo", "Central", "Circle", "District",
                  "Hammersmith & City", "Jubilee", "Metropolitan",
                  "Northern", "Piccadilly", "Victoria", "Waterloo"])

bracket_regex = re.compile('\[.+?\]')


def main():
    # read LondonUndergroundstations.csv
    base_data = get_base_data("LondonUndergroundStations.csv")
    base_data_stations = set(base_data['Station'])

    # read openstreetmap_data.csv
    lat_long = get_lat_long("openstreetmap_data.csv")
    lat_long_stations = set(lat_long['Station'])

    # read circuit diagram locations
    circuit_diagram = get_circuit_diagram_from_csv("digitiser")
    circuit_diagram_stations = set(circuit_diagram['Station'])

    # read station depths
    depths = get_station_depths('station-depths.csv')
    depths_stations = set(depths['Station'])

    # read station depths
    survey = get_survey_data('PassengerCount2009.csv')
    survey_stations = set(depths['Station'])

    # Count stations in each dataset
    print "Base_data_stations n = {}".format(len(base_data_stations))
    print "lat_long_stations n = {}".format(len(lat_long_stations))
    print "circuit_diagram_stations n = {}".format(len(circuit_diagram_stations))
    print "depths_stations n = {}".format(len(depths_stations))

    # Show stations missing
    missing_lat_long = base_data_stations.difference(lat_long_stations)
    missing_x_y = base_data_stations.difference(circuit_diagram_stations)
    missing_depths = base_data_stations.difference(depths_stations)
    missing_survey = base_data_stations.difference(survey_stations)

    print "Missing lat/long:\n {}".format(missing_lat_long)
    print "Missing x/y:\n {}".format(missing_x_y)
    print "Missing depths \n {}".format(missing_depths)
    print "Missing survey \n {}".format(missing_survey)

    # Add spatial coordinates to base_data
    base_data = add_in_lat_long(base_data, lat_long)
    base_data = add_in_x_y(base_data, circuit_diagram)
    base_data = add_in_depths(base_data, depths)

    # Normalise xy coordinates
    base_data = normalise_xy(base_data)
    print base_data[0:10]

    # Add a "distance on line field"?

    # Output summary file
    base_data.to_csv("processed_data.csv")

    # Output path file for circuit diagram
    circuit_diagram_path_file = make_path_file(circuit_diagram, base_data)
    circuit_diagram_path_file = normalise_path_xy(
        circuit_diagram_path_file, base_data)

    # Add base_data to path
    circuit_diagram_path_file = add_base_data_to_path(circuit_diagram_path_file, base_data)

    # Add survey data to path
    # circuit_diagram_path_file = add_survey_data_to_path(circuit_diagram_path_file, survey)


    circuit_diagram_path_file.to_csv('circuit_diagram_path_file.csv')


def add_survey_data_to_path(circuit_diagram_path_file, survey):
    survey.rename(columns={"Line": "line"}, inplace=True)
    circuit_diagram_path_file = pandas.merge(circuit_diagram_path_file, survey, how='left', on=['Station','line'])
    return circuit_diagram_path_file

def get_survey_data(filename):
    survey = pandas.read_csv(filename)
    #depth_data.rename(columns={"Station Name": "Station"}, inplace=True)
    #depth_data.rename(
    #    columns={"Ground Level outside Station": "Ground level"}, inplace=True)
    # Rename Waterloo & City to Waterloo
    #depth_data.rename(columns={"Waterloo & City": "Waterloo"}, inplace=True)
    # Depth data has no circle line
    # Drop unwanted columns
    return survey

def add_base_data_to_path(circuit_diagram_path_file, base_data):
    base_data = base_data.drop('latitude', 1)
    base_data = base_data.drop('longitude', 1)
    base_data = base_data.drop('x', 1)
    base_data = base_data.drop('y', 1)
    circuit_diagram_path_file = pandas.merge(circuit_diagram_path_file, base_data, on='Station')
    
    return circuit_diagram_path_file

def normalise_path_xy(circuit_diagram_path_file, base_data):
    lmax = max(base_data['longitude'])
    lmin = min(base_data['longitude'])
    xmax = max(circuit_diagram_path_file['X'])
    xmin = min(circuit_diagram_path_file['X'])

    #circuit_diagram_path_file['X'] = (circuit_diagram_path_file['X']-xmin)/(xmax-xmin) + lmin
    circuit_diagram_path_file['X'] = (lmax - lmin) * (
        numpy.float64(circuit_diagram_path_file['X']) - xmin) / (xmax - xmin) + lmin

    lmax = min(base_data['latitude'])
    lmin = max(base_data['latitude'])
    ymax = max(circuit_diagram_path_file['Y'])
    ymin = min(circuit_diagram_path_file['Y'])
    circuit_diagram_path_file['Y'] = (lmax - lmin) * (
        numpy.float64(circuit_diagram_path_file['Y']) - ymin) / (ymax - ymin) + lmin
    return circuit_diagram_path_file


def make_path_file(circuit_diagram, base_data):
    # Station X Y id line
    # line is actually the filename from which the line came
    columns = ('Station', 'line_branch', 'line', 'path_order',  
               'X', 'Y', 'latitude', 'longitude')
    path = pandas.DataFrame(
        columns=columns)

    path_order = 1
    for each_row in circuit_diagram.iterrows():
        lat,lng = get_lat_and_lng_from_base_data(base_data, each_row[1]['Station'], each_row[1]['line'])
        line = get_line_from_filename(each_row[1]['line'])
    
        row = pandas.DataFrame([dict(Station=each_row[1]['Station'], 
                                line_branch=each_row[1]['line'], 
                                line=line, path_order=path_order, 
                                X=each_row[1]['X'], Y=each_row[1]['Y'], 
                                latitude=lat, longitude=lng)])
        path = path.append(row, ignore_index=True)
        path_order += 1

    return path

def get_lat_and_lng_from_base_data(base_data, station, filename):
    lat = base_data[base_data['Station'] == station]['latitude']
    lng = base_data[base_data['Station'] == station]['longitude']
    
    if len(lat) == 0:
        print filename, station

    try:
        lat = list(lat)[0]
        lng = list(lng)[0]
    except IndexError:
        lat = numpy.float64('NaN')
        lng = numpy.float64('NaN')

    return lat, lng

def get_line_from_filename(filename):
    line_part = filename.split('-line')
    assert len(line_part) == 2, "filename doesn't split in two on '-line'"
    line = line_part[0].capitalize()
    if line == 'Hammersmith-and-city':
        line = 'Hammersmith & City'
    return line

def add_in_depths(base_data, depths):
    altitude = []
    depth = []
    station_names = base_data['Station']
    for station in station_names:
        new_altitude = depths[depths['Station'] == station]['Ground level']

            # print station
        altitude.extend(new_altitude)
        tmp = depths[depths['Station'] == station]
        raw_row = tmp.irow(0)
        new_depth = get_average_depth_from_raw(raw_row)
        depth.extend([new_depth])

    base_data['ground level'] = altitude
    base_data['depth'] = depth

    return base_data

def get_average_depth_from_raw(raw_row):
    level = 0
    count = 0
    for label, value in raw_row.T.iteritems():
        if label in ['Station', 'Ground level']:
            continue
        if isinstance(value, str) and value == ' ':
            value = float('nan')
        try:
            if label in line_names and not math.isnan(float(value)):
                level += float(value)
                count += 1
        except TypeError:
            print "Could not convert '{}' in '{}'".format(value, raw_row)
    if count != 0:
    # Subtract 100m from rail level - the datum is 100m below sea level
        average_level = level / count
     # Subtract rail level from altitude
        average_depth = raw_row['Ground level'] - (average_level - 100)
    else:
        average_depth = 0
    return average_depth


def get_station_depths(filename):
    depth_data = pandas.read_csv(filename)
    depth_data.rename(columns={"Station Name": "Station"}, inplace=True)
    depth_data.rename(
        columns={"Ground Level outside Station": "Ground level"}, inplace=True)
    # Rename Waterloo & City to Waterloo
    depth_data.rename(columns={"Waterloo & City": "Waterloo"}, inplace=True)
    # Depth data has no circle line
    # Drop unwanted columns
    return depth_data


def get_base_data(filename):
    base_data = pandas.read_csv(filename, index_col='rowid')
    # Drop unwanted columns
    base_data = base_data.drop(
        ["Principal Line (intermediate)", "Principal Line",
         "MainlineOpened", "OtherNameS",
         "Number of Records", "Photo"], 1)
    # Normalise Line names
    # Reduce to standard form, concatenate with | identify first line, make
    # count
    base_data = normalise_line_names(base_data)
    return base_data


def get_lat_long(filename):
    lat_long = pandas.read_csv("openstreetmap_data.csv")
    # Rename Name field to Station
    lat_long.rename(columns={"Name": "Station"}, inplace=True)
    return lat_long


def get_circuit_diagram_from_svg(filename):
    circuit_diagram_json = json.load(open(filename))
    # This cleans up names in circuit_diagram which have an \n in them
    for key in circuit_diagram_json.keys():
        if '\n' in key:
            circuit_diagram_json[
                key.replace('\n', ' ')] = circuit_diagram_json.pop(key)

    circuit_diagram = pandas.DataFrame([
        [key, circuit_diagram_json[key]['x'],
         circuit_diagram_json[key]['y']]
        for key in circuit_diagram_json.keys()])

    circuit_diagram = circuit_diagram.rename(
        columns={0: "Station", 1: "x", 2: "y"}, inplace=True)
    return circuit_diagram


def get_circuit_diagram_from_csv(directory):
    circuit_diagram = pandas.DataFrame()
    files = glob.glob(join(directory + '\\*line-stations-branch*.*'))
    for input_file in files:
        path, filename = split(input_file)
        # print filename
        this_frame = pandas.read_csv(input_file, sep='\t')
        this_frame['line'] = filename

        circuit_diagram = pandas.concat([circuit_diagram, this_frame])
    return circuit_diagram


def add_in_lat_long(base_data, lat_long):
    lat = []
    lng = []
    station_names = base_data['Station']
    for station in station_names:
        new_lat = lat_long[lat_long['Station'] == station]['Latitude']
        if len(new_lat) > 1:
            pass
            # print station
        lat.extend(new_lat)
        lng.extend(lat_long[lat_long['Station'] == station]['Longitude'])

    base_data['latitude'] = lat
    base_data['longitude'] = lng

    return base_data


def add_in_x_y(base_data, circuit_diagram):
    x = []
    y = []
    station_names = base_data['Station']
    for station in station_names:
        x_matches = list(
            circuit_diagram[circuit_diagram['Station'] == station]['X'])
        y_matches = list(
            circuit_diagram[circuit_diagram['Station'] == station]['Y'])
        if len(x_matches) > 1:
            pass
            # print station
        new_x = x_matches[0]
        new_y = y_matches[0]

        # if len(new_x)>1:
        #    print station
        x.append(new_x)
        y.append(new_y)

    #print("Length of base data: {}".format(len(base_data)))
    #print("Length of x: {}".format(len(x)))

    # print x
    base_data['x'] = x
    base_data['y'] = y
    return base_data


def normalise_line_names(base_data):
    # We're going to update the dataframe in place and add fields
    nlines = []
    clean_lines = []
    first_line = []
    for entry in base_data['LineS']:
        # Apply regex which removes [*]
        entry = bracket_regex.sub('', entry)
        # Splitting on double space seems to do the trick
        # (Splitting on single space fails on Hammersmith and City)
        line_list = entry.split("  ")
        # Make a column of lens
        nlines.append(len(line_list))
        # Rejoin with |
        clean_lines.append("|".join(line_list))
        # Update columns
        date_line_list = {}
        for line in line_list:
            date_line_list[line] = opening_dates[line]
        
        first_line.append(min(date_line_list, key=date_line_list.get))

    base_data['nlines'] = nlines
    base_data['LineS'] = clean_lines
    base_data['first_line'] = first_line
    return base_data


def write_data_to_csv(filename, data):
    with open(filename, 'wb') as f:
        writer = csv.DictWriter(f, OUTPUT_COLUMNS)
        writer.writeheader()
        for line in data:
            # print line
            # writer.writerow(line)
            for field in line.keys():
                line[field] = line[field].encode('utf-8')
            writer.writerow(line)


def normalise_xy(base_data):
    lmax = max(base_data['longitude'])
    lmin = min(base_data['longitude'])
    xmax = max(base_data['x'])
    xmin = min(base_data['x'])

    base_data['x'] = (lmax - lmin) * \
        (base_data['x'] - xmin) / (xmax - xmin) + lmin

    lmax = min(base_data['latitude'])
    lmin = max(base_data['latitude'])
    ymax = max(base_data['y'])
    ymin = min(base_data['y'])
    base_data['y'] = (lmax - lmin) * \
        (base_data['y'] - ymin) / (ymax - ymin) + lmin
    return base_data

if __name__ == '__main__':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    main()
