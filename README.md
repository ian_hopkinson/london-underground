# London Underground mapping

`rmstations.json` and `tubestations.json` are from 
[https://github.com/benbarnett/SVG-Tube-Map/](here)

Not quite clear what the difference between these two jsons, they originate from scrapedstations.php in the
_assets directory and give the x,y coordinates of stations on the "circuit diagram" map

`openstreetmap_data.csv` is from [http://wiki.openstreetmap.org/wiki/London_Tube_Stations](here)
`LondonUndergroundStations.csv` is from [http://en.wikipedia.org/wiki/List_of_London_Underground_stations](here)
`Station depths` is from [http://www.ianvisits.co.uk/blog/2011/07/01/how-deep-is-every-tube-station-on-the-underground/](here)
Final data table I'm looking for is:

Station | Line(s) | nlines | Lat | Long | x | y | DateOpened | LocalAuthority | Zone | Usage

Station with the most lines is KX with 6

I modified the Bank and Monument figures, they are listed separately as stations but share traffic figures, so
I divided by two and assigned one each
